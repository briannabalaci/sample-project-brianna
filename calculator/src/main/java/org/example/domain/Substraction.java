package org.example.domain;


public class Substraction extends Expression {

    public Substraction(Operation operation, double[] args) {
        super(operation, args);
    }

    @Override
    public double executeSimpleOperation(double[] args) {
        return args[0]-args[1];
    }
}
