package org.example.domain;



public abstract class Expression {
    private Operation operation;
    private double[] args ;

    public Expression(Operation operation, double[] args) {
        this.operation = operation;
        this.args = args;
    }

    public abstract double executeSimpleOperation(double[] arguments) throws Exception;

    public double execute()throws Exception{
        double rez ;
            rez = executeSimpleOperation(args);

        return rez;
    }

}
