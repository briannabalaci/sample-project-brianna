package org.example.domain;



public class Multiplication extends Expression {
    public Multiplication(Operation operation, double[] args) {
        super(operation, args);
    }

    @Override
    public double executeSimpleOperation(double[] args) {
        return args[0]*args[1];
    }
}
