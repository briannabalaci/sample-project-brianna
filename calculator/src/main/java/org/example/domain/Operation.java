package org.example.domain;

public enum Operation {
    ADDITION,
    SUBSTRACTION,
    MULTIPLICATION,
    DIVISION,

    MAXIMUM,
    MINIMUM,
    SQUAREROOT
}

