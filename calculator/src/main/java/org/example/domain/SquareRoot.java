package org.example.domain;

public class SquareRoot extends Expression {


    public SquareRoot(Operation operation, double[] args) {
        super(operation, args);
    }
    @Override
    public double executeSimpleOperation(double[] args) {
        return Math.sqrt(args[0]);
    }
}
