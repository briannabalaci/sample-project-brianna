package org.example.domain;

public class Addition extends Expression {


    public Addition(Operation operation, double[] args) {
        super(operation, args);
    }
    @Override
    public double executeSimpleOperation(double[] args) {
        return args[0] + args[1];
    }
}
