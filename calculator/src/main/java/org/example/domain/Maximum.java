package org.example.domain;

public class Maximum extends Expression {


    public Maximum(Operation operation, double[] args) {
        super(operation, args);
    }
    @Override
    public double executeSimpleOperation(double[] args) {
        if (args[0] >= args[1] )
        return args[0] ;
        else return args[1];
    }
}
