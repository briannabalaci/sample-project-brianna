package org.example.domain;



public  class Division extends Expression {
    public Division(Operation operation, double[] args) {
        super(operation, args);
    }

    @Override
    public double executeSimpleOperation(double[] args)throws Exception{
        return args[0]/args[1];
    }
}
