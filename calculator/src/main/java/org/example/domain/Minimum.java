package org.example.domain;

public class Minimum extends Expression {


    public Minimum(Operation operation, double[] args) {
        super(operation, args);
    }
    @Override
    public double executeSimpleOperation(double[] args) {
        if (args[0] > args[1] )
            return args[1] ;
        else return args[0];
    }
}