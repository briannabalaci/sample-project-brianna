package org.example.UI;
import org.example.Parser.ExpressionParser;
import org.example.domain.Expression;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserInterface {
    private BufferedReader reader;
    public UserInterface( ) {
        reader = new BufferedReader(
                new InputStreamReader(System.in));
    }

    public void run() throws IOException {
        while (true) {
            System.out.println("Introduceti primul operator: ");
            String arg1 = reader.readLine();
            if (arg1.equals("stop"))
                break;
            System.out.println("Introduceti operatia: ");
            String op = reader.readLine();
            if (op.equals("stop"))
                break;
            String arg2 = "";
            if(!op.equals("sqrt")) {
                System.out.println("Introduceti al doilea operator: ");
                arg2 = reader.readLine();
                if (arg2.equals("stop"))
                    break;
            }
            else{
                arg2 = arg1;
            }
            ExpressionParser parser = new ExpressionParser(arg1,arg2,op);

            if (parser.validate_operators() == false) {
                System.out.println("Expresia nu este corecta!");
            } else if (parser.validate_operation() == false) {
                System.out.println("Operatori incorecti!");
            } else {
                Expression compl = parser.expresieFinala();
                try {
                    double nr = compl.execute();
                    System.out.println(nr);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}

