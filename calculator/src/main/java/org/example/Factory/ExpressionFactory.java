package org.example.Factory;


import org.example.domain.*;

public class ExpressionFactory {
    private static ExpressionFactory instance = null;
    private ExpressionFactory() {
    }
    public static ExpressionFactory getInstance(){
        if(instance == null)
            instance = new ExpressionFactory();
        return instance;
    }

    public Expression createExpression(Operation operation, double[] args) {
        switch (operation) {
            case ADDITION:
                return new Addition(operation, args);
            case SUBSTRACTION:
                return new Substraction(operation, args);
            case MULTIPLICATION:
                return new Multiplication(operation, args);
            case DIVISION:
                return new Division(operation, args);
            case MAXIMUM:
                return new Maximum(operation, args);
            case MINIMUM:
                return new Minimum(operation, args);
            case SQUAREROOT:
                return new SquareRoot(operation, args);
            default:
                return null;
        }

    }








}


