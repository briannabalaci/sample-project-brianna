package org.example.Parser;


import org.example.Factory.ExpressionFactory;
import org.example.domain.Expression;
import org.example.domain.Operation;

public class ExpressionParser {

    private String[] args;
    private String operation;


    public ExpressionParser(String arg1, String arg2, String operation) {
        this.args = new String[2];
        this.args[0] = arg1;
        this.args[1] = arg2;
        this.operation = operation;
    }

    public Operation returnOperator(){
        if (this.operation.equals("+")){
            return Operation.ADDITION;
        }
        if (this.operation.equals("-") ){
            return Operation.SUBSTRACTION;
        }
        if (this.operation.equals("/") ){
            return Operation.DIVISION;
        }
        if (this.operation.equals("*") ){
            return Operation.MULTIPLICATION;
        }
        if (this.operation.equals("max")){
            return Operation.MAXIMUM;
        }
        if (this.operation.equals("min")){
            return Operation.MINIMUM;
        }

        return Operation.SQUAREROOT;

    }
    public boolean validate_operation( ) {
        String operator = this.operation;
        if (operator.equalsIgnoreCase("+") == false && operator.equalsIgnoreCase("-") == false && operator.equalsIgnoreCase("/") == false && operator.equalsIgnoreCase("*") == false && operator.equalsIgnoreCase("max") == false && operator.equalsIgnoreCase("min") == false  && operator.equalsIgnoreCase("sqrt") == false   )
            return false;
        return true;
    }
    public boolean validate_operators(){
        for (int i = 0; i< this.args.length; i++)
            if(!args[i].matches("[-]?[0-9]+") && !args[i].matches("[-]?[0-9]+[.]{1}[0-9]+") )
                return false;

        return true;
    }

    double[] double_operators(){
        double[] doubles = new double[2];
        doubles[0] = Double.parseDouble(args[0]);
        doubles[1] = Double.parseDouble(args[1]);
        return doubles;
    }


    public Expression expresieFinala(){
        return ExpressionFactory.getInstance().createExpression(returnOperator(),double_operators());
    }



}

